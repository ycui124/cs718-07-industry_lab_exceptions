package ictgradschool.industry.lab_exceptions.ex05;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)
        System.out.print("Enter a string of at most 100 characters: ");
        try {
            getCharacters();
        } catch (ExceedMaxStringLengthException e) {
            System.out.println(e.getMessage());
        } catch (InvalidWordException e) {
            System.out.println(e.getMessage());
        }
    }

    // TODO Write some methods to help you.
    private void getCharacters() throws ExceedMaxStringLengthException, InvalidWordException {

        String characters = Keyboard.readInput();
        if (characters.length() > 100) {
            throw new ExceedMaxStringLengthException("more than 100 characters");
        }
        String[] strArr = characters.split(" ");
//        System.out.println(strArr[0] + strArr.length);
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && !(strArr[i].equals(""))) {
                String firstWord= strArr[i].substring(0, 1);
                char charFirst=firstWord.charAt(0);
                if (!((charFirst >= 65 && charFirst <= 90) || (charFirst >= 97 && charFirst <= 122))) {
                    System.out.println();
                    throw new InvalidWordException("contains invalid words");
                }
                System.out.print(firstWord+" ");
            }
        }

    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
